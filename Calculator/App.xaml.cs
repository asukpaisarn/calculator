using System.Diagnostics;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Calculator
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new CalculatorPage();
        }

        protected override void OnStart()
        {
            Debug.WriteLine("OnStart() called");
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            Debug.WriteLine("OnSleep() called");
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            Debug.WriteLine("OnSleep() called");
            // Handle when your app resumes
        }
    }
}

