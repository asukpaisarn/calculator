using System;
using Xamarin.Forms;

namespace Calculator
{
    public partial class CalculatorPage : ContentPage
    {

        string entry = "";
        string number1 = "";
        string number2 = "";
        char operation;
        double result = 0.0;

        public CalculatorPage()
        {
            InitializeComponent();

        }
        /** CLICKING ON NUMBERS 7 THROUGH 9 **/
        private void Seven_Clicked(object sender, EventArgs e)
        {
            this.EntryBox.Text = "";
            entry += "7";
            this.EntryBox.Text += entry;

        }
        private void Eight_Clicked(object sender, EventArgs e)
        {
            this.EntryBox.Text = "";
            entry += "8";
            this.EntryBox.Text += entry;

        }
        private void Nine_Clicked(object sender, EventArgs e){
            this.EntryBox.Text = "";
            entry += "9";
            this.EntryBox.Text += entry;
        }
        /** CLICKING ON DIVIDE**/
        private void Divide_Clicked(object sender, EventArgs e){
            number1 = entry;
            operation = '/';
            entry = "";
        }
        /** CLICKING ON NUMBERS 4 THROUGH 6 **/
        private void Four_Clicked(object sender, EventArgs e){
            this.EntryBox.Text = "";
            entry += "4";
            this.EntryBox.Text += entry;
        }
        private void Five_Clicked(object sender, EventArgs e){
            this.EntryBox.Text = "";
            entry += "5";
            this.EntryBox.Text += entry;
        }
        private void Six_Clicked(object sender, EventArgs e){
            this.EntryBox.Text = "";
            entry += "6";
            this.EntryBox.Text += entry;
        }
        /** CLICKING ON MULTIPLY **/
        private void Multiply_Clicked(object sender, EventArgs e){
            number1 = entry;
            operation = '*';
            entry = "";
        }

        /** CLICKING ON NUMBERS 1 THROUGH 3 **/
        private void One_Clicked(object sender, EventArgs e){
            this.EntryBox.Text = "";
            entry += "1";
            this.EntryBox.Text += entry;
        }
        private void Two_Clicked(object sender, EventArgs e){
            this.EntryBox.Text = "";
            entry += "2";
            this.EntryBox.Text += entry;
        }
        private void Three_Clicked(object sender, EventArgs e){
            this.EntryBox.Text = "";
            entry += "3";
            this.EntryBox.Text += entry;
        }
        /** CLICKING ON MINUS **/
        private void Minus_Clicked(object sender, EventArgs e){
            number1 = entry;
            operation = '-';
            entry = "";
        }
        /** CLICKING ON ZERO **/
        private void Zero_Clicked(object sender, EventArgs e){
            this.EntryBox.Text = "";
            entry += "0";
            this.EntryBox.Text += entry;
        }
        /** CLICKING ON ADD **/
        private void Add_Clicked(object sender, EventArgs e){
            number1 = entry;
            operation = '+';
            entry = "";
        }
        /** CLICKING ON EQUAL **/
        private void Equal_Clicked(object sender, EventArgs e)
        {
            number2 = entry;
            double first;
            double second;
            double.TryParse(number1, out first);
            double.TryParse(number2, out second);
            if (operation == '*')
            {
                result = first * second;
                EntryBox.Text = result.ToString();
            }
            else if (operation == '+')
            {
                result = first + second;
                EntryBox.Text = result.ToString();
            }
            else if(operation == '-')
            {
                result = first - second;
                EntryBox.Text = result.ToString();
            }
            else if(operation == '/')
            {
                if (second != 0)
                {
                    result = first / second;
                    EntryBox.Text = result.ToString();
                }
                else
                {
                    EntryBox.Text = "Undefined";
                }
            }
        }
        /** CLICKING ON CLEAR **/
        private void Clear_Clicked(object sender, EventArgs e){
            this.EntryBox.Text = "";
            this.entry = "";
            this.number1 = "";
            this.number2 = "";
        }


    }
}
